package socialnetwork.GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.config.ApplicationContext;
import socialnetwork.controllers.IntroducereController;
import socialnetwork.domain.Angajat;
import socialnetwork.domain.Pontaj;
import socialnetwork.domain.Sarcina;
import socialnetwork.domain.Tuple;
import socialnetwork.repository.Repository;
import socialnetwork.repository.file.AngajatiFileRepository;
import socialnetwork.repository.file.PontajFileRepository;
import socialnetwork.repository.file.SarcinaFileRepository;
import socialnetwork.service.AngajatService;
import socialnetwork.service.PontajService;
import socialnetwork.service.SarcinaService;

import java.io.IOException;


public class MainFX extends Application {
    private static AngajatService angajatService;
  
    private static SarcinaService sarcinaService;
    private static PontajService pontajService;



    @Override
    public void start(Stage primaryStage) throws Exception {
           initView(primaryStage);
            primaryStage.setWidth(600);
            primaryStage.setTitle("Angajatii");
            primaryStage.show();
    }

    public static void main(String[] args){
        String fileNameAngajati=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.angajati");
        String fileNamePontaje=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.pontaje");
        String fileNameSarcini=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.sarcini");


        Repository<String, Angajat> angajatFileRepository = new AngajatiFileRepository( fileNameAngajati);
        Repository<String, Sarcina> sarcinaFileRepository = new SarcinaFileRepository( fileNameSarcini);
        Repository<Tuple<String,String>, Pontaj> pontajFileRepository = new PontajFileRepository( fileNamePontaje);

        angajatService=new AngajatService(angajatFileRepository);
        sarcinaService=new SarcinaService(sarcinaFileRepository);
        pontajService=new PontajService(pontajFileRepository,angajatFileRepository,sarcinaFileRepository);



        launch(args);
    }

    private void initView(Stage primaryStage) throws IOException {
        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(getClass().getResource("/views/introducere.fxml"));
        AnchorPane layout = loader.load();
        primaryStage.setScene(new Scene(layout));
        IntroducereController introducereController=loader.getController();
        introducereController.setAngajatService(angajatService,primaryStage);
        introducereController.setSarcinaService(sarcinaService);
        introducereController.setPontajService(pontajService);
        introducereController.setIntroductionStage(primaryStage);





    }
}
