package socialnetwork.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import socialnetwork.domain.*;
import socialnetwork.service.AngajatService;
import socialnetwork.service.PontajService;
import socialnetwork.service.SarcinaService;

import java.util.*;
import java.util.stream.Collectors;

public class IntroducereController {


    AngajatService angajatService;
    SarcinaService sarcinaService;
    PontajService pontajService;
    ObservableList<Angajat> modelAngajati= FXCollections.observableArrayList();
    ObservableList<AngajatDTO> modelAngajatiDTO= FXCollections.observableArrayList();
    Stage primaryStage;

    @FXML
    TableView<Angajat> idTable;

    @FXML
    TableColumn<Angajat,String> idNume;

    @FXML
    TableColumn<Angajat,Float> idVenitPeOra;
    @FXML
    TableColumn<Angajat, Nivel> idNivel;
    @FXML
    TextField idGrea;
    @FXML
    TextField idMedie;
    @FXML
    TextField idUsoara;
    @FXML
    Button idButtonMedia;
   @FXML
   ComboBox idComboBox;
    @FXML
    Button idRap;
    @FXML
    TableView<AngajatDTO> idTable2;

    @FXML
    TableColumn<AngajatDTO,String> idNume2;

    @FXML
    TableColumn<AngajatDTO,Nivel> idNivel2;
    @FXML
    TableColumn<AngajatDTO, Float> idSalar;


    @FXML
    public void initialize(){
        idNume.setCellValueFactory(new PropertyValueFactory<Angajat,String>("Nume"));
        idVenitPeOra.setCellValueFactory(new PropertyValueFactory<Angajat,Float>("VenitPeOra"));
      idNivel.setCellValueFactory(new PropertyValueFactory<Angajat,Nivel>("Nivel"));
        idNume2.setCellValueFactory(new PropertyValueFactory<AngajatDTO,String>("Nume"));
        idNivel2.setCellValueFactory(new PropertyValueFactory<AngajatDTO,Nivel>("Nivel"));
        idSalar.setCellValueFactory(new PropertyValueFactory<AngajatDTO,Float>("Salar"));

        idTable2.setItems(modelAngajatiDTO);
        idTable.setItems(modelAngajati);

    }

    public void initModel()
    {
        modelAngajati.setAll((Collection<? extends Angajat>) angajatService.getAll());
    }
    public void setAngajatService(AngajatService angajatService, Stage primaryStage) {
        this.angajatService=angajatService;
        this.primaryStage=primaryStage;
    }

    public void setSarcinaService(SarcinaService sarcinaService) {
        this.sarcinaService=sarcinaService;
    }

    public void setPontajService(PontajService pontajService) {
        this.pontajService=pontajService;
        initModel();
    }

    public void setIntroductionStage(Stage primaryStage) {
        this.primaryStage=primaryStage;
    }

    public void filtrareAngajat() {
        Iterable<Angajat> list= angajatService.getAll();
        List<Angajat>lista=new ArrayList<>();


        list.forEach(a->lista.add(a));// debug
       // modelAngajati.forEach(System.out::println);
        modelAngajati.setAll(lista.stream()
              .sorted(Comparator.comparing(Angajat::getNivel)
                      .thenComparing(Comparator.comparing(Angajat::getVenitPeOra).reversed()))
               .collect(Collectors.toList()));


    }






    public void revin() {
        initModel();
    }

    public void afisareMedia() {

        List<Sarcina>sarcinaUsoare=sarcinaService.getSarcinaDificultate(Dificultate.Usoara);

        List<Sarcina>sarcinaGrea=sarcinaService.getSarcinaDificultate(Dificultate.Grea);
        List<Sarcina>sarcinaMedia=sarcinaService.getSarcinaDificultate(Dificultate.Medie);
        Iterable<Sarcina>iterable=sarcinaService.getAll();
        List<Sarcina> sarcini=new ArrayList<>();
        iterable.forEach(sarcina -> sarcini.add(sarcina));

        Map<Dificultate,List<Sarcina>> map=sarcini.stream()
                .collect(Collectors.groupingBy(x->x.getDificultate())); //grupez dupa dificultate


        map.entrySet()
                .forEach(x->{
                   double media=x.getValue()
                           .stream()
                           .mapToDouble(n->n.getNrOreEstimate())
                           .average()
                           .orElse(0);
                       if(x.getKey().equals(Dificultate.Usoara))
                           idUsoara.setText(String.valueOf(media));

                    if(x.getKey().equals(Dificultate.Medie))
                        idMedie.setText(String.valueOf(media));

                    if(x.getKey().equals(Dificultate.Grea))
                        idGrea.setText(String.valueOf(media));

                });

        map.entrySet().forEach(System.out::println);

    }

    public void afisareHarnici() {
        Iterable<Angajat>angajatiToti=angajatService.getAll();
        pontajService.harniciL();
        modelAngajatiDTO.setAll(pontajService.harniciL() );

    }


    public void raport() {


        int luna =  Integer.parseInt((String) idComboBox.getSelectionModel().getSelectedItem());
        modelAngajatiDTO.setAll(pontajService.raport(luna));
    }


    public void selectComboBox() {
        idComboBox.getItems().setAll("1","2","3","4","5","6","7","8","9","10","11","12");
    }
}
