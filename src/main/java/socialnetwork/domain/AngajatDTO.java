package socialnetwork.domain;

import java.util.Objects;

public class AngajatDTO extends Entity<String> {

    private String nume;
    private Nivel nivel;
    private float Salar;

    public AngajatDTO(String nume, Nivel nivel, float salar) {
        this.nume = nume;
        this.nivel = nivel;
        Salar = salar;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public Nivel getNivel() {
        return nivel;
    }

    public void setNivel(Nivel nivel) {
        this.nivel = nivel;
    }

    public float getSalar() {
        return Salar;
    }

    public void setSalar(int salar) {
        Salar = salar;
    }

    @Override
    public String toString() {
        return "AngajatDTO{" +
                "nume='" + nume + '\'' +
                ", nivel=" + nivel +
                ", Salar=" + Salar +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AngajatDTO that = (AngajatDTO) o;
        return Salar == that.Salar &&
                Objects.equals(nume, that.nume) &&
                nivel == that.nivel;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nume, nivel, Salar);
    }
}
