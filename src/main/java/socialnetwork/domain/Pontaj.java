package socialnetwork.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

public class Pontaj extends Entity<Tuple<String,String>> {

    private LocalDateTime data;

    public Pontaj(LocalDateTime data) {
        this.data = data;
    }


    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pontaj pontaj = (Pontaj) o;
        return Objects.equals(data, pontaj.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }


    @Override
    public String toString() {
        return "Pontaj{" +
                "idLeft"+super.getId().getLeft()+" "
                +"idRight"+super.getId().getRight()+" "+
                "data=" + data +
                '}';
    }
}
