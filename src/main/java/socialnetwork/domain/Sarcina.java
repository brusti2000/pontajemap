package socialnetwork.domain;

import java.util.Objects;

public class Sarcina extends  Entity<String>{

    private Dificultate dificultate;
    private int nrOreEstimate;

    public Sarcina(Dificultate dificultate, int nrOreEstimate) {
        this.dificultate = dificultate;
        this.nrOreEstimate = nrOreEstimate;
    }

    public Dificultate getDificultate() {
        return dificultate;
    }

    public void setDificultate(Dificultate dificultate) {
        this.dificultate = dificultate;
    }

    public int getNrOreEstimate() {
        return nrOreEstimate;
    }

    public void setNrOreEstimate(int nrOreEstimate) {
        this.nrOreEstimate = nrOreEstimate;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sarcina sarcina = (Sarcina) o;
        return nrOreEstimate == sarcina.nrOreEstimate &&
                dificultate == sarcina.dificultate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(dificultate, nrOreEstimate);
    }

    @Override
    public String toString() {
        return "Sarcina{" +
                "dificultate=" + dificultate +
                ", nrOreEstimate=" + nrOreEstimate +
                '}';
    }
}
