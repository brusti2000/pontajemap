package socialnetwork.repository.file;



import socialnetwork.domain.Angajat;
import socialnetwork.domain.Nivel;
import socialnetwork.repository.Repository;

import java.util.List;

public class AngajatiFileRepository extends AbstractFileRepository<String, Angajat>{

    public AngajatiFileRepository(String fileName) {
        super(fileName);
    }

    @Override
    public Angajat extractEntity(List<String> attributes) {
        Angajat angajat = new Angajat(attributes.get(1),Float.parseFloat(attributes.get(2)),Nivel.valueOf(attributes.get(3)));
        angajat.setId(attributes.get(0)); //idul

        return angajat;

    }

    @Override
    protected String createEntityAsString(Angajat entity) {
        return entity.getId()+","+entity.getNume()+","+entity.getVenitPeOra()+","+entity.getNivel();
    }
}
