package socialnetwork.repository.file;

import socialnetwork.domain.Angajat;
import socialnetwork.domain.Pontaj;
import socialnetwork.domain.Tuple;
import socialnetwork.repository.Repository;
import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.List;

public class PontajFileRepository extends AbstractFileRepository<Tuple<String,String>, Pontaj> {


    public PontajFileRepository(String fileName) {
        super(fileName);
    }


    @Override
    public Pontaj extractEntity(List<String> attributes) {
        LocalDateTime data=LocalDateTime.parse(attributes.get(2),Constants.DATE_TIME_FORMATER);
        Pontaj pontaj=new Pontaj(data);
        String leftId=attributes.get(0);
        String rightId=attributes.get(1);
        pontaj.setId(new Tuple(leftId,rightId));
        return pontaj;
    }

    @Override
    protected String createEntityAsString(Pontaj entity) {
        return entity.getId().getLeft()+","+entity.getId().getRight()+","+entity.getData().format(Constants.DATE_TIME_FORMATER);

    }
}
