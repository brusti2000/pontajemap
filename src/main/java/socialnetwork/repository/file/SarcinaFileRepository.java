package socialnetwork.repository.file;

import socialnetwork.domain.Angajat;
import socialnetwork.domain.Dificultate;
import socialnetwork.domain.Nivel;
import socialnetwork.domain.Sarcina;
import socialnetwork.repository.Repository;

import java.util.List;

public class SarcinaFileRepository  extends AbstractFileRepository<String, Sarcina> {

    public SarcinaFileRepository(String fileName) {
        super(fileName);
    }
    @Override
    public Sarcina extractEntity(List<String> attributes) {
        Sarcina  sarcina= new Sarcina(Dificultate.valueOf(attributes.get(1)),Integer.parseInt(attributes.get(2)));
        sarcina.setId(attributes.get(0)); //idul

        return sarcina;

    }

    @Override
    protected String createEntityAsString(Sarcina entity) {
        return entity.getId()+","+entity.getDificultate()+","+entity.getNrOreEstimate();
    }

}
