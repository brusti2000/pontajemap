package socialnetwork.service;

import socialnetwork.domain.Angajat;
import socialnetwork.repository.Repository;


public class AngajatService {
    private final Repository<String, Angajat> repoAngajat;




    public AngajatService(Repository<String, Angajat> repoAngajat) {
        this.repoAngajat = repoAngajat;
    }

    /**
     * @param Angajat - type Angajat, entity that has to be added
     * @return user of type Angajat
     */
    public Angajat addAngajat(Angajat Angajat) {
        Angajat user = repoAngajat.save(Angajat);
        //facem validarea in functie de ce ne returneaza .save
        return user;
    }

    /**
     * method that removes the user with id given as a parameter and
     * its friendships
     * @param id - Long, the unique identifier of the user to be deleted
     * @return user that has been removed
     */
    public Angajat deleteAngajat(String id) {
        //3;4
        //3;5
        //6;3
        //sterge in toate prieteniile care il contin pe 3
        Angajat Angajat = repoAngajat.delete(id);


        return Angajat;
    }

    /**
     * @return all the users
     */
    public Iterable<Angajat> getAll(){
        return repoAngajat.findAll();
    }
    public Angajat findOne(String ID){
        return repoAngajat.findOne(ID);
    }



}
