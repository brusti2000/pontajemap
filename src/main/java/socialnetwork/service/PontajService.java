package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.repository.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class PontajService {
    private Repository<Tuple<String, String>, Pontaj> repositoryPontaj;
    private Repository<String, Angajat> repoAngajat;
    private Repository<String, Sarcina> repoSarcina;
    private List<AngajatDTO>angajatiDTO=new ArrayList<>();


    public PontajService(Repository<Tuple<String, String>, Pontaj> repositoryPontaj, Repository<String, Angajat> repoAngajat, Repository<String, Sarcina> repoSarcina) {
        this.repositoryPontaj = repositoryPontaj;
        this.repoAngajat = repoAngajat;
        this.repoSarcina = repoSarcina;
    }

    public Pontaj addPontaj(Pontaj pontajParam) {
        Pontaj pontaj = repositoryPontaj.save(pontajParam);
        return pontaj;
    }

    public Pontaj deletePontaj(Tuple<String, String> ids) {
        //3;4
        //3;5
        //6;3
        //sterge in toate prieteniile care il contin pe 3
        Pontaj pontaj = repositoryPontaj.delete(ids);


        return pontaj;
    }

    /**
     * @return all the users
     */
    public Iterable<Pontaj> getAll() {
        return repositoryPontaj.findAll();
    }

    public Pontaj getPontaj(String idLeft, String idRight) {

        return repositoryPontaj.findOne(new Tuple<>(idLeft, idRight));


    }

    public List<AngajatDTO> harniciL(){

        Iterable<Angajat> iterable = repoAngajat.findAll();
        List<Angajat> angajati = new ArrayList<>();
        iterable.forEach(angajati::add);
        Iterable<Pontaj> iterable2 = repositoryPontaj.findAll();
        List<Pontaj> pontaje = new ArrayList<>();
        iterable2.forEach(pontaje::add);
        List<AngajatDTO> angajatiDTO = new ArrayList<>();
        for(Angajat a : angajati){
            float nrOre = 0;
            for(Pontaj p : pontaje){
                if(p.getId().getLeft().equals(a.getId())){
                    Sarcina sarcina = repoSarcina.findOne(p.getId().getRight());
                    nrOre += sarcina.getNrOreEstimate();
                }
            }
            float salar = nrOre * a.getVenitPeOra();
            AngajatDTO angajatDTO = new AngajatDTO(a.getNume(),a.getNivel(),salar);
            angajatiDTO.add(angajatDTO);
        }

        List<AngajatDTO> sorted =
                angajatiDTO.stream()
                        .sorted(Comparator.comparing(AngajatDTO::getSalar).reversed())
                        .collect(Collectors.toList());
        System.out.println(sorted.stream().limit(2).collect(Collectors.toList()));
        return sorted.stream().limit(2).collect(Collectors.toList());

    }

    public List<AngajatDTO> raport(int Luna){
        Iterable<Angajat> iterable = repoAngajat.findAll();
        List<Angajat> angajati = new ArrayList<>();
        iterable.forEach(angajati::add);
        Iterable<Pontaj> iterable2 = repositoryPontaj.findAll();
        List<Pontaj> pontaje = new ArrayList<>();
        iterable2.forEach(pontaje::add);
        List<AngajatDTO> angajatiDTO = new ArrayList<>();
        for(Angajat a : angajati){
            float nrOre = 0;
            for(Pontaj p : pontaje){

                if(p.getId().getLeft().equals(a.getId()) && p.getData().getMonthValue()==Luna){
                    Sarcina sarcina = repoSarcina.findOne(p.getId().getRight());
                    nrOre += sarcina.getNrOreEstimate();
                }
            }
            float salar = nrOre * a.getVenitPeOra();
            AngajatDTO angajatDTO = new AngajatDTO(a.getNume(),a.getNivel(),salar);
            angajatiDTO.add(angajatDTO);
        }


        List<AngajatDTO> sorted =
                angajatiDTO.stream()
                        .sorted(Comparator.comparing(AngajatDTO::getNivel)
                        .thenComparing(Comparator.comparing(AngajatDTO::getSalar)))
                        .collect(Collectors.toList());
        System.out.println(angajatiDTO);
        return sorted;
    }


}
