package socialnetwork.service;

import socialnetwork.domain.Angajat;
import socialnetwork.domain.Dificultate;
import socialnetwork.domain.Sarcina;
import socialnetwork.repository.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class SarcinaService {
    private Repository<String, Sarcina> repositorySarcina;

    public SarcinaService(Repository<String, Sarcina> repositorySarcina) {

        this.repositorySarcina = repositorySarcina;
    }


    public Sarcina addSarcina(Sarcina SarcinaParam){
        Sarcina Sarcina = repositorySarcina.save(SarcinaParam);
        return Sarcina;
    }
    public Sarcina deleteSarcina(String id) {
        //3;4
        //3;5
        //6;3
        //sterge in toate prieteniile care il contin pe 3
        Sarcina sarcina = repositorySarcina.delete(id);


        return sarcina;
    }

    /**
     * @return all the users
     */
    public Iterable<Sarcina> getAll(){
        return repositorySarcina.findAll();
    }

    public List<Sarcina> getSarcinaDificultate(Dificultate dif){
        Iterable<Sarcina>iterable= repositorySarcina.findAll();
     List<Sarcina> sarcini=new ArrayList<>();
     iterable.forEach(sarcina -> sarcini.add(sarcina));

    sarcini.stream()
             .filter(x->x.getDificultate().equals(dif));

     return sarcini;

    }

    public Sarcina getSarcina(String idSarcina){
        return repositorySarcina.findOne(idSarcina);
    }
}
